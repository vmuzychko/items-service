* Test task requested by customer

# Project Name

Items service.
Unmarshalling and marshalling based on JAXB.
Scheme validation.
Custom adapters for Date and Enum.
Build tool Maven.

## Installation

In a development environment or console, use **mvn install** to build and install artifact into the local repository.

## Usage

Use ItemService to deserialize or serialize data from/to xml. Methods deserializeItems and serializeItems should be used respectively.

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History

Current version 1.0-SNAPSHOT

