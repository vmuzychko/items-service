package common;


public class ItemsServiceException extends RuntimeException{
    public ItemsServiceException(String message, Throwable cause) {
        super("Items service exception: " + message, cause);
    }
}
