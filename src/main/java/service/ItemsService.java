package service;


import common.ItemsServiceException;
import model.Items;
import org.xml.sax.SAXException;
import validation.SchemaValidationEventHandler;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Service stands for marshalling and unmarshalling collection of items
 * wrapped in Items root element and schema validation.
 */
public class ItemsService {

    public static final String SCHEMA_XSD = "schema.xsd";

    /**
     * Deserializes an xml input stream into collection of items
     * wrapped in Items root element.
     * Unmarshalling is interrupted in case of schema validation errors.
     *
     * @param xmlInput xml input stream
     * @return Items root element
     */
    public Items deserializeItems(InputStream xmlInput) {
        try {
            return (Items) createUnmarshaller().unmarshal(xmlInput);
        } catch (JAXBException e) {
            throw new ItemsServiceException("Unable to unmarshal items", e);
        }
    }

    /**
     * Serializes collection of items wrapped in Items root element
     * into output stream.
     * Marshalling is interrupted in case of schema validation errors.
     *
     * @param items collection of items wrapped in Items root element
     * @return output stream
     */
    public OutputStream serializeItems(Items items) {
        try {
            ByteArrayOutputStream xmlOutput = new ByteArrayOutputStream();
            createMarshaller().marshal(items, xmlOutput);
            return xmlOutput;
        } catch (JAXBException e) {
            throw new ItemsServiceException("Unable to marshal items", e);
        }
    }

    /**
     * Constructs an unmarshaller initialized with schema and SchemaValidationEventHandler.
     *
     * @return unmarshaller initialized with schema and SchemaValidationEventHandler
     * @throws JAXBException
     */
    private Unmarshaller createUnmarshaller() throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Items.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setSchema(createSchema());
        unmarshaller.setEventHandler(new SchemaValidationEventHandler());
        return unmarshaller;
    }

    /**
     * Constructs a marshaller initialized with schema and SchemaValidationEventHandler.
     *
     * @return marshaller initialized with schema and SchemaValidationEventHandler
     * @throws JAXBException
     */
    private Marshaller createMarshaller() throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Items.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setSchema(createSchema());
        marshaller.setEventHandler(new SchemaValidationEventHandler());
        return marshaller;
    }

    /**
     * Creates an xsd schema using xsd file.
     *
     * @return schema created with xsd file
     */
    private Schema createSchema() {
        try {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            return factory.newSchema(getSchemaFile());
        } catch (SAXException e) {
            throw new ItemsServiceException("Unable to create xsd schema", e);
        }
    }

    /**
     * Returns xsd schema file.
     *
     * @return xsd schema file.
     */
    private File getSchemaFile() {
        return new File(getClass().getClassLoader().getResource(SCHEMA_XSD).getFile());
    }
}
