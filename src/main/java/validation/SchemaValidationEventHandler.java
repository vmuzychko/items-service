package validation;


import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;

/**
 * Prevents marshalling/unmarshalling from further processing in case of validation error
 * Writes event message to console log
 */
public class SchemaValidationEventHandler implements ValidationEventHandler {

    @Override
    public boolean handleEvent(ValidationEvent event) {
        System.out.println(event.getMessage());
        return false;
    }
}
