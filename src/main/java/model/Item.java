package model;

import adapters.DateAdapter;
import adapters.ItemTypeAdapter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;

@XmlType(propOrder = {"name", "date", "type"})
@XmlRootElement(name = "item")
public class Item {

    private String name;

    private Date date;

    private ItemType type;

    public String getName() {
        return name;
    }

    @XmlElement(name = "name")
    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    @XmlJavaTypeAdapter(DateAdapter.class)
    @XmlElement(name  = "date")
    public void setDate(Date date) {
        this.date = date;
    }

    public ItemType getType() {
        return type;
    }

    @XmlJavaTypeAdapter(ItemTypeAdapter.class)
    @XmlElement(name  = "type")
    public void setType(ItemType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        if (name != null ? !name.equals(item.name) : item.name != null) return false;
        if (date != null ? !date.equals(item.date) : item.date != null) return false;
        return type == item.type;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Item{");
        sb.append("name='").append(name).append('\'');
        sb.append(", date=").append(date);
        sb.append(", type=").append(type);
        sb.append('}');
        return sb.toString();
    }
}
