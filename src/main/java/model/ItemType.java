package model;

/**
 * Describes possible types of items
 */
public enum ItemType {
    DROPDOWN("DR"),
    MULTICHOICE("ML"),
    TEXT("TX");

    private String code;

    ItemType(String code) {
        this.code = code;
    }

    /**
     * Returns item type by it's code provided
     * @param code item code
     * @return ItemType
     */
    public static ItemType getByCode(String code) {

        for (ItemType type : values()) {
            if (type.getCode().equals(code)) {
                return type;
            }
        }

        throw new IllegalArgumentException("Item type with code " + code + " doesn't exist");
    }

    public String getCode() {
        return code;
    }
}
