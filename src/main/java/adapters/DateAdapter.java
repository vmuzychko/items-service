package adapters;


import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Adapts a Date type for marshaling/unmarshalling into/from String with custom date format
 */
public class DateAdapter extends XmlAdapter<String, Date> {

    public static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
    public static final SimpleDateFormat FORMATTER = new SimpleDateFormat(DEFAULT_DATE_FORMAT);

    public Date unmarshal(String stringDate) throws ParseException {
        return FORMATTER.parse(stringDate);
    }

    public String marshal(Date date) {
        return FORMATTER.format(date);
    }
}
