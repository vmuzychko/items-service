package adapters;


import model.ItemType;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Adapts a ItemType enum type for marshaling/unmarshalling into/from String code
 */
public class ItemTypeAdapter extends XmlAdapter<String, ItemType> {
    public ItemType unmarshal(String code) {
        return ItemType.getByCode(code);
    }

    public String marshal(ItemType type) {
        return type.getCode();
    }
}
