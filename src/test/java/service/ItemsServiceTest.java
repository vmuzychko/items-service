package service;


import common.ItemsServiceException;
import model.Item;
import model.ItemType;
import model.Items;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ItemsServiceTest {

    public static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
    public static final SimpleDateFormat FORMATTER = new SimpleDateFormat(DEFAULT_DATE_FORMAT);

    private ItemsService itemService = new ItemsService();

    @Test
    public void shouldDeserializeItems() throws Exception {
        Items items = itemService.deserializeItems(readXml("valid-items.xml"));

        List<Item> itemList = items.getItems();
        Item first = itemList.get(0);
        assertEquals(2, itemList.size());

        assertEquals("some name", first.getName());
        assertEquals(FORMATTER.parse("18/10/2016"), first.getDate());
        assertEquals(ItemType.TEXT, first.getType());

        Item second = itemList.get(1);
        assertEquals("another name", second.getName());
        assertEquals(FORMATTER.parse("22/11/2016"), second.getDate());
        assertEquals(ItemType.DROPDOWN, second.getType());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWithNullSource() throws Exception {
        itemService.deserializeItems(null);
    }

    @Test(expected = ItemsServiceException.class)
    public void shouldThrowExceptionWithInvalidSource() throws Exception {
        itemService.deserializeItems(readXml("invalid-content.xml"));
    }

    @Test(expected = ItemsServiceException.class)
    public void shouldNotDeserializeInvalidName() throws Exception {
        itemService.deserializeItems(readXml("invalid-name.xml"));
    }

    @Test(expected = ItemsServiceException.class)
    public void shouldNotDeserializeInvalidType() throws Exception {
        itemService.deserializeItems(readXml("invalid-type.xml"));
    }

    @Test(expected = ItemsServiceException.class)
    public void shouldNotDeserializeInvalidDate() throws Exception {
        itemService.deserializeItems(readXml("invalid-date.xml"));
    }

    @Test
    public void shouldSerializeItems() throws Exception {
        Items items = new Items();
        Item first = new Item();
        first.setName("some name");
        first.setDate(FORMATTER.parse("18/10/2016"));
        first.setType(ItemType.TEXT);
        items.addItem(first);

        Item second = new Item();
        second.setName("another name");
        second.setDate(FORMATTER.parse("22/11/2016"));
        second.setType(ItemType.DROPDOWN);
        items.addItem(second);

        String actual = normalize(itemService.serializeItems(items).toString());

        String expected = normalize(new String(IOUtils.toByteArray(readXml("valid-items.xml"))));

        assertEquals(expected, actual);
    }

    @Test(expected = ItemsServiceException.class)
    public void shouldNotSerializeInvalidItems() throws Exception {
        Items items = new Items();
        Item first = new Item();
        first.setName("some name ssssssssssssssssssssssssssssssssssss");
        first.setDate(FORMATTER.parse("18/10/2016"));
        first.setType(ItemType.TEXT);
        items.addItem(first);

        itemService.serializeItems(items);
    }

    private String normalize(String expected) {
        return expected.replace("\n", "").replace("\r", "").replace(" ", "");
    }

    private InputStream readXml(String fileName) throws Exception {
        return getClass().getClassLoader().getResourceAsStream(fileName);
    }
}