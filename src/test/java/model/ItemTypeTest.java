package model;

import org.junit.Assert;
import org.junit.Test;

public class ItemTypeTest {
    @Test
    public void shouldReturnTypeByCode() throws Exception {
        Assert.assertEquals(ItemType.DROPDOWN, ItemType.getByCode("DR"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionByNullCode() throws Exception {
        ItemType.getByCode(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionByInvalidCode() throws Exception {
        ItemType.getByCode("INVALID");
    }
}